var first_draw = 1;
var linechart = function(selector, data, xlabel, ylabel) {
  var chart = nv.models.lineChart()
    .useInteractiveGuideline(true)
    ;

  chart.xAxis
    .axisLabel(xlabel)
    .tickFormat(d3.format(',r'))
    ;

  chart.yAxis
    .axisLabel(ylabel)
    .tickFormat(d3.format('.02f'))
    ;

  d3.select(selector)
    .datum(data)
    .transition().duration(500)
    .call(chart)
    ;

  nv.utils.windowResize(chart.update);

  return chart;
}

var parkinson_task_vis= function(data){
    var html_str =
            '<div class="col-lg-8">'+
                '<div id="history-visualization">'+
                    '<div id="history-diagram-'+data['level_id']+'" data-level-id="'+data['level_id']+'"></div>'+
                '</div>'+
            '</div>';
    var htmlcontent = $("#task-visualization-body");
    htmlcontent.html(html_str);
    
  //nv.addGraph(linechart('#chart svg', data, 'Time (ms)', 'Voltage (v)'));

  // ===================== Start of Template ========================
    var _diagramWrapperTemplate = '\
        <div class="diagram-graph-wrapper">\
            <div class="diagram-axis-y"></div>\
            <div class="diagram-graph"></div>\
            <div class="diagram-axis-x"></div>\
            <div class="diagram-legend"></div>\
        </div>\
    ';
    // ---------------------- End of Template -------------------------


    // ============= Start of vis app core functions ==================

    // config
    var appGraphs = {};
    var diagramConfig = [];
    function addDiagram(options) {
        var defaultOptions = {
            id: "0",
            renderer: "line",
            interpolation: "linear",
            min: "auto",
            series: []
        };

        options = $.extend(defaultOptions, options);

        var $diagramWrapper = $(_diagramWrapperTemplate);
        
        $("#history-diagram-" + options.id).append($diagramWrapper);

        var $diagramGraph = $diagramWrapper.find(".diagram-graph");

        // build the diagram
        var graph = new Rickshaw.Graph({
            element: $diagramGraph[0],
            renderer: options.renderer,
            interpolation: options.interpolation,
            min: options.min,
            series: options.series
        });

        // add axis
        //var xAxes = new Rickshaw.Graph.Axis.Time({
        var xAxes = new Rickshaw.Graph.Axis.X({
            graph: graph,
            orientation: 'bottom',
            element: $diagramWrapper.find(".diagram-axis-x")[0]
        });
        var yAxis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            orientation: 'left',
            element: $diagramWrapper.find(".diagram-axis-y")[0]
        });

        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            xFormatter: String
        });

        // add legend
        var legend = new Rickshaw.Graph.Legend({
            graph: graph,
            element: $diagramWrapper.find(".diagram-legend")[0]
        });
        var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
            graph: graph,
            legend: legend
        });
        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
            graph: graph,
            legend: legend
        });

        // series data is saved as 2nd level reference, with may be reordered by rickshaw during the legend hover behavior
        appGraphs[options.id] = {
            graph: graph,
            seriesData: $.map(options.series, function(d, i) { return [d.data]; })
        };

        graph.render();
        resizeGraph($diagramGraph);
    }

    //var dc = '[{"series":[{"color":"#0cc","data":[{"y":82.23426164404593,"x":1},{"y":56.40336099141868,"x":2},{"y":78.16543350880443,"x":3},{"y":55.25891670846089,"x":4},{"y":72.66838781618854,"x":5},{"y":128.37520371898924,"x":6},{"y":26.235399001392665,"x":7},{"y":60.56737231370737,"x":8},{"y":57.86814928185484,"x":9},{"y":50.59788135289965,"x":10},{"y":22.24031192059345,"x":11},{"y":54.44426838213349,"x":12},{"y":54.29459240214368,"x":13},{"y":68.10641608336687,"x":14},{"y":76.00017666357132,"x":15},{"y":116.06108364026744,"x":16},{"y":57.238555688939705,"x":17},{"y":68.93972376897408,"x":18},{"y":50.841596878612904,"x":19},{"y":39.8649905394474,"x":20},{"y":64.4316851825871,"x":21},{"y":57.439482770198424,"x":22},{"y":48.839353499274225,"x":23},{"y":116.14502164519278,"x":24},{"y":86.38445172157803,"x":25},{"y":55.69093097223766,"x":26},{"y":162.29591515307783,"x":27},{"y":69.16943325252475,"x":28},{"y":70.27024353670974,"x":29},{"y":85.62671204249187,"x":30}],"name":"avg (easy)"},{"color":"#c0c","data":[{"y":44.2538022743214,"x":1},{"y":185.1399986213834,"x":2},{"y":261.3527437704278,"x":3},{"y":53.99577407692023,"x":4},{"y":54.87419016239952,"x":5},{"y":43.590083045056275,"x":6},{"y":62.752808219177496,"x":7},{"y":49.50703140175453,"x":8},{"y":101.36890074197277,"x":9},{"y":59.43042863803316,"x":10},{"y":91.23925372129851,"x":11},{"y":76.84258490405465,"x":12},{"y":57.14308887091961,"x":13},{"y":46.531954099769976,"x":14},{"y":87.12976190301544,"x":15},{"y":68.8795769136104,"x":16},{"y":39.558899039090186,"x":17},{"y":89.15197922961929,"x":18},{"y":77.04824544546528,"x":19},{"y":57.10960673810871,"x":20},{"y":76.33326252242526,"x":21},{"y":20.74086978171824,"x":22},{"y":67.32237856438954,"x":23},{"y":57.958996731612835,"x":24},{"y":76.11459979144524,"x":25},{"y":78.87784672886951,"x":26},{"y":69.03795536575086,"x":27},{"y":144.9150854066502,"x":28},{"y":81.03437090570029,"x":29},{"y":71.44620285818928,"x":30}],"name":"avg (medium)"},{"color":"#cc0","data":[{"y":64.97304609251589,"x":1},{"y":222.25194420244054,"x":2},{"y":77.22149315297663,"x":3},{"y":61.03919893708519,"x":4},{"y":34.17318924339091,"x":5},{"y":49.039344121503845,"x":6},{"y":82.03071600171391,"x":7},{"y":50.41602153163015,"x":8},{"y":71.56405635021589,"x":9},{"y":47.42959450471488,"x":10},{"y":70.63127092330943,"x":11},{"y":54.64771022598938,"x":12},{"y":80.53606444268122,"x":13},{"y":74.59921955952179,"x":14},{"y":54.455099862422315,"x":15},{"y":103.87656188385097,"x":16},{"y":46.86586907656121,"x":17},{"y":67.20516088565454,"x":18},{"y":61.041640717354234,"x":19},{"y":147.134077721544,"x":20},{"y":63.480439932263906,"x":21},{"y":173.90145102232722,"x":22},{"y":40.96088106922348,"x":23},{"y":99.53981810039173,"x":24},{"y":58.92797005513538,"x":25},{"y":116.68931980911368,"x":26},{"y":196.01576927377758,"x":27},{"y":136.40635641562415,"x":28},{"y":89.16431008031036,"x":29},{"y":76.76180993366857,"x":30}],"name":"avg (hard)"},{"color":"red","data":[{"y":80,"x":1},{"y":80,"x":30}],"name":"sync threshold"}],"id":1,"min":0},{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3},{"y":-0.6739134387351872,"x":4},{"y":-6.3428571428571265,"x":5},{"y":-1.979487692307693,"x":6},{"y":-17.387888484848485,"x":7},{"y":-28.541966783216775,"x":8},{"y":-1.028573186813185,"x":9},{"y":-12.424252121212124,"x":10},{"y":-11.766666666666682,"x":11},{"y":16.45105209790209,"x":12},{"y":-3.4901963235293922,"x":13},{"y":17.67912417582417,"x":14},{"y":-4.1928564285714245,"x":15},{"y":-2.9406615384615344,"x":16},{"y":1.5549434065934318,"x":17},{"y":1.9392851071428596,"x":18},{"y":26.148354945054937,"x":19},{"y":-7.898904615384603,"x":20},{"y":-22.24999999999997,"x":21},{"y":33.872718787878775,"x":22},{"y":15.401102197802217,"x":23},{"y":35.59340604395605,"x":24},{"y":-29.042422969696922,"x":25},{"y":51.52308417582419,"x":26},{"y":-24.460598787878784,"x":27},{"y":-31.9727272727273,"x":28},{"y":22.676923076923085,"x":29}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[{"y":1,"x":1},{"y":1,"x":2},{"y":1,"x":3},{"y":1,"x":4},{"y":0.75,"x":5},{"y":1,"x":6},{"y":0.75,"x":7},{"y":1,"x":8},{"y":0.6,"x":9},{"y":1,"x":10},{"y":1,"x":11},{"y":1,"x":12},{"y":1,"x":13},{"y":1,"x":14},{"y":1,"x":15},{"y":1,"x":16},{"y":1,"x":17},{"y":0.75,"x":18},{"y":0.375,"x":19},{"y":1,"x":20}],"name":"success rate (easy)"},{"color":"#c0c","data":[{"y":1,"x":1},{"y":1,"x":2},{"y":1,"x":3},{"y":1,"x":4},{"y":1,"x":5},{"y":1,"x":6},{"y":1,"x":7},{"y":0.6666666666666666,"x":8},{"y":0.8,"x":9},{"y":0.5,"x":10},{"y":0.8,"x":11},{"y":1,"x":12},{"y":0.8,"x":13},{"y":1,"x":14},{"y":1,"x":15},{"y":0.6666666666666666,"x":16},{"y":1,"x":17},{"y":1,"x":18},{"y":1,"x":19},{"y":1,"x":20}],"name":"success rate (medium)"},{"color":"#cc0","data":[{"y":0.29411764705882354,"x":1},{"y":0.5555555555555556,"x":2},{"y":0.8333333333333334,"x":3},{"y":0.4166666666666667,"x":4},{"y":0.8333333333333334,"x":5},{"y":1,"x":6},{"y":1,"x":7},{"y":0.7142857142857143,"x":8},{"y":1,"x":9},{"y":0.4166666666666667,"x":10},{"y":1,"x":11},{"y":1,"x":12},{"y":0.8333333333333334,"x":13},{"y":0.7142857142857143,"x":14},{"y":1,"x":15},{"y":1,"x":16},{"y":1,"x":17},{"y":1,"x":18},{"y":0.38461538461538464,"x":19},{"y":1,"x":20}],"name":"success rate (hard)"}],"id":3,"min":0},{"series":[{"color":"#0cc","data":[{"y":0.9166666666666666,"x":1},{"y":1,"x":2},{"y":0.5,"x":3},{"y":1,"x":4},{"y":0.6923076923076923,"x":5},{"y":0.5454545454545454,"x":6},{"y":1,"x":7},{"y":0.25,"x":8},{"y":0.25,"x":9},{"y":0.25,"x":10},{"y":1,"x":11},{"y":0.625,"x":12},{"y":0.1111111111111111,"x":13},{"y":1,"x":14},{"y":1,"x":15},{"y":0.8461538461538461,"x":16},{"y":1,"x":17},{"y":1,"x":18},{"y":0.7272727272727273,"x":19},{"y":1,"x":20},{"y":1,"x":21}],"name":"success rate (easy)"},{"color":"#c0c","data":[{"y":0.8461538461538461,"x":1},{"y":0.4375,"x":2},{"y":0.7692307692307693,"x":3},{"y":0.5,"x":4},{"y":0.38461538461538464,"x":5},{"y":0.6,"x":6},{"y":0.6153846153846154,"x":7},{"y":0.6470588235294118,"x":8},{"y":0.7857142857142857,"x":9},{"y":0.8333333333333334,"x":10},{"y":0.7857142857142857,"x":11},{"y":0.7058823529411765,"x":12},{"y":0.15384615384615385,"x":13},{"y":1,"x":14},{"y":0.7857142857142857,"x":15},{"y":0.13333333333333333,"x":16},{"y":0.8235294117647058,"x":17},{"y":0.7142857142857143,"x":18},{"y":0.6875,"x":19},{"y":1,"x":20},{"y":1,"x":21}],"name":"success rate (medium)"},{"color":"#cc0","data":[{"y":0.8888888888888888,"x":1},{"y":0.8,"x":2},{"y":0.8695652173913043,"x":3},{"y":1,"x":4},{"y":0.3333333333333333,"x":5},{"y":0.47368421052631576,"x":6},{"y":0.34782608695652173,"x":7},{"y":0.8421052631578947,"x":8},{"y":1,"x":9},{"y":0.5833333333333334,"x":10},{"y":0.8260869565217391,"x":11},{"y":1,"x":12},{"y":0.5238095238095238,"x":13},{"y":0.8947368421052632,"x":14},{"y":0.8421052631578947,"x":15},{"y":0.047619047619047616,"x":16},{"y":0.8181818181818182,"x":17},{"y":0.8636363636363636,"x":18},{"y":0.7619047619047619,"x":19},{"y":0.65,"x":20},{"y":0.5555555555555556,"x":21}],"name":"success rate (hard)"}],"id":4,"min":0}]';
    //var dc = '[{"series":[{"color":"#0cc","data":[],"name":"avg (easy)"},{"color":"#c0c","data":[],"name":"avg (medium)"},{"color":"#cc0","data":[],"name":"avg (hard)"}],"id":1,"min":0},{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[],"name":"success rate (easy)"},{"color":"#c0c","data":[],"name":"success rate (medium)"},{"color":"#cc0","data":[],"name":"success rate (hard)"}],"id":3,"min":0},{"series":[{"color":"#0cc","data":[],"name":"success rate (easy)"},{"color":"#c0c","data":[],"name":"success rate (medium)"},{"color":"#cc0","data":[],"name":"success rate (hard)"}],"id":4,"min":0}]';
    //var dc   = '[{"series":[{"color":"#0cc","data":[{"y":3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[{"y":3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[{"y":3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[{"y":3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2}]';
    

    var dc   = '[{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":1},{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":2},{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":3},{"series":[{"color":"#0cc","data":[{"y":-3.096969151515208,"x":1},{"y":8.769607843137244,"x":2},{"y":-11.428698709677429,"x":3}],"name":"Diameter trend"}],"id":4}]';
    //diagramConfig = JSON.parse(dc);
    //console.log(dc);
    
    var dce = data["d"][0];

    var dc = [];
    for(ii =0;ii<4;ii++){
        //dce["id"]=ii;
        if(ii==0){
            dce = data["d"][0]
        }else if(ii==1){
            dce = data["d"][1]
        }
        var newo = JSON.parse(JSON.stringify(dce));
        newo["id"] = ii+1;
        dc.push(newo);
    }
    diagramConfig = dc;


    $.each(diagramConfig, function(i, d) {
        addDiagram(d);
    });

    setupEventTriggers();
    
    function setupEventTriggers() {
        $(window).on("resize", function() {
            $(".diagram-graph").each(function(i, d) {
                resizeGraph($(this));
            });
        });
    }

    function resizeGraph($diagramGraph) {
        // swap hack for jquery width() with hidden node
        var $tabPane = $diagramGraph.parents(".tab-pane");
        if (!$tabPane.hasClass("active")) {
            $tabPane.addClass("swap-hack");
        }

        var graphObj = appGraphs[$diagramGraph.parent().parent().data("level-id")];
        if (graphObj && graphObj.hasOwnProperty("graph")) {
            graphObj.graph.configure({
                width: ($diagramGraph.parent().width()
                            - $diagramGraph.siblings(".diagram-legend").outerWidth()
                            - $diagramGraph.siblings(".diagram-axis-y").outerWidth() - 10)
            });
            graphObj.graph.render();
        }

        // swap hack reset
        $tabPane.removeClass("swap-hack");
            }
}

// end of task visualization







//start of play visualization
// runtime data
var appDataId = 0;
var appData = null;
var appGraphs = {};
var minX = minY = 5000;
var maxX = maxY = 0;
var parkinson_play_vis= function(data){

        
    var html_str =      '<h2>Data Collected</h2>'+
                        '<div id="diagrams-container"></div>'+
                        '<div id="diagram-description"></div>';
    var htmlcontent = $("#play-visualization-body");
    htmlcontent.html(html_str);
    data.is_checked = true;
    //data.level = "/api/v1/level/"+$scope.selected_level.id+"/"
    data = JSON.stringify(data);

    data = JSON.parse(data);
    


    // ===================== Start of Template ========================
    var _diagramWrapperTemplate = '\
            <div class="well diagram-wrapper">\
                <div class="row">\
                    <h4 class="span4"></h4>\
                    <div class="btn-group pull-right">\
                        <a class="btn play-diagram" href="#"><i class="icon-play"></i> Play</a>\
                    </div>\
                </div>\
                <div class="diagram-graph-wrapper">\
                    <div class="diagram-axis-y"></div>\
                    <div class="diagram-graph"></div>\
                    <div class="diagram-axis-x"></div>\
                    <div class="diagram-legend"></div>\
                </div>\
            </div>';
    // ---------------------- End of Template -------------------------


    // ============= Start of vis app core functions ==================

    // config
    var diagramConfig = {
        "/api/v1/level/1/": {
            "id": 1,
            "title": "Task synchronization",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "easy (left)", "color": "#0cc", "source": "grass3", "dataX": "xleft", "dataY": "yleft" },
                { "name": "easy (right)", "color": "#0cc", "source": "grass3", "dataX": "xright", "dataY": "yright" },
                { "name": "medium (left)", "color": "#c0c", "source": "grass5", "dataX": "xleft", "dataY": "yleft" },
                { "name": "medium (right)", "color": "#c0c", "source": "grass5", "dataX": "xright", "dataY": "yright" },
                { "name": "hard (left)", "color": "#cc0", "source": "grass7", "dataX": "xleft", "dataY": "yleft" },
                { "name": "hard (right)", "color": "#cc0", "source": "grass7", "dataX": "xright", "dataY": "yright" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/2/": {
            "id": 2,
            "title": "Micrographia",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "movement", "color": "#0cc", "source": "waterPosition", "dataX": "x", "dataY": "y" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/3/": {
            "id": 3,
            "title": "Short time memory(number)",
            "renderer": "line",
            "interpolation": "step-after",
            "series": [
                { "name": "easy", "color": "#0cc", "source": "animal3", "dataX": "time", "dataY": "serial" },
                { "name": "medium", "color": "#c0c", "source": "animal4", "dataX": "time", "dataY": "serial" },
                { "name": "hard", "color": "#cc0", "source": "animal5", "dataX": "time", "dataY": "serial" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Horizontal axis shows the time in seconds, while vertial axis shows the animals by their labeled number.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/4/": {
            "id": 4,
            "title": "Short time memory(2D path)",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "easy (actual)", "color": "#0cc", "source": "crop0position", "dataX": "x", "dataY": "y" },
                { "name": "easy (correct)", "color": "#0ea", "source": "crop0correct", "dataX": "serialx", "dataY": "serialy" },
                { "name": "medium (actual)", "color": "#c0c", "source": "crop1position", "dataX": "x", "dataY": "y" },
                { "name": "medium (correct)", "color": "#a0e", "source": "crop1correct", "dataX": "serialx", "dataY": "serialy" },
                { "name": "hard (actual)", "color": "#cc0", "source": "crop2position", "dataX": "x", "dataY": "y" },
                { "name": "hard (correct)", "color": "#ea0", "source": "crop2correct", "dataX": "serialx", "dataY": "serialy" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        }
    };

    appDataId = data.id;
    loadAppData();

    function loadAppData() {


        // parse json string to object
        try {
            data.data = JSON.parse(data.data);
        }
        catch(error) {
        }
        appData = data;

        if (diagramConfig.hasOwnProperty(appData["level"]) && diagramConfig[appData["level"]].hasOwnProperty("preprocess")) {

            diagramConfig[appData["level"]]["preprocess"](appData.data);
        }

        // generate diagrams
        var diagramOptions = getDiagramOptions();
        addDiagram(diagramOptions);
        

        // show description
        if (diagramOptions !== null) {
            $("#diagram-description").html(diagramConfig[appData["level"]]["description"]);
        }




    }

    function getDiagramOptions() {
        var levelUri = appData["level"];
        if (!diagramConfig.hasOwnProperty(levelUri)) {
            console.log("The level is unknown!");
            return null;
        }
        var dc = diagramConfig[levelUri];

        var diagramOptions = {
            id: dc.id,
            title: dc.title,
            renderer: dc.renderer,
            interpolation: dc.interpolation,
            series: []
        };

        $.each (dc.series, function(i, d) {
            var seriesObj = {
                name: d.name,
                color: d.color,
                data: []
            };

            // pop up the data array
            $.each(appData.data[d.source], function(ii, dd) {
                seriesObj.data.push({
                    "x": +dd[d.dataX],
                    "y": +dd[d.dataY]
                });

                var x = parseInt(dd[d.dataX]);
                var y = parseInt(dd[d.dataY]);
                if(x < minX)
                    minX = x;
                if(x > maxX)
                    maxX = x;
                if(y < minY)
                    minY = y;
                if(y > maxY)
                    maxY = y;
            });
            diagramOptions.series.push(seriesObj);
        });
        return diagramOptions;
    }

    // crate (append) a new diagram if there is no one having the same data-diagram-id
    // or remove the existing one, then create one at the same place
    function addDiagram(options) {

        var defaultOptions = {
            id: "0",
            title: "Diagram",
            renderer: "line",
            interpolation: "linear",
            series: []
        };

        if (typeof options == "object") {
            options = $.extend(defaultOptions, options);
        } else {
            options = defaultOptions;
        }

        var diagramId = "diagram" + options.id;
        var $diagramWrapper = $(_diagramWrapperTemplate).attr("data-diagram-id", options.id);
        var $existingDiagramWrapper = $(".diagram-wrapper[data-diagram-id=" + options.id +"]");

        if ($existingDiagramWrapper.length > 0) {
            // replace the first existing diagram with new one (assuming that it's the only one)
            $existingDiagramWrapper.first().before($diagramWrapper).remove();
        }
        else {
            // append a new one
            $("#diagrams-container").append($diagramWrapper);
        }


        
        $diagramWrapper.find("h4").text(options.title);
        var $diagramGraph = $diagramWrapper.find(".diagram-graph");

        // build the diagram
        var graph = new Rickshaw.Graph({
            element: $diagramGraph[0],
            renderer: options.renderer,
            series: options.series,
            rangeX: [minX, maxX],
            rangeY: [minY, maxY],
            //min: "auto",
            interpolation: options.interpolation
        });

        // add axis
        var xAxes = new Rickshaw.Graph.Axis.X({
            graph: graph,
            orientation: 'bottom',
            element: $diagramWrapper.find(".diagram-axis-x")[0]
        });
        var yAxis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            orientation: 'left',
            element: $diagramWrapper.find(".diagram-axis-y")[0]
        });

        if (appData["level"] === "/api/v1/level/3/") {
            // show hover details for 3rd level
            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                xFormatter: function(x) { return x + " seconds" },
                formatter: function(series, x, y) {
                    return '<span>Animal Label:</span> ' + (y || 'None') + '<br>' +
                        '<span>Time:</span> ' + x + ' seconds';
                }
            });
        }

        // add legend
        var legend = new Rickshaw.Graph.Legend({
            graph: graph,
            element: $diagramWrapper.find(".diagram-legend")[0]
        });
        var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
            graph: graph,
            legend: legend
        });
        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
            graph: graph,
            legend: legend
        });

        // series data is saved as 2nd level reference, with may be reordered by rickshaw during the legend hover behavior
        appGraphs[options.id] = {
            graph: graph,
            seriesData: $.map(options.series, function(d, i) { return [d.data]; })
        };

        graph.render();
        resizeGraph($diagramGraph);
    }

    function setupEventTriggers() {

        $("#diagrams-container").on("click", ".play-diagram", function() {
            var $playBtn = $(this);
            if ($playBtn.hasClass("disabled"))
                return false;

            var diagramId = $(this).parents(".diagram-wrapper").attr("data-diagram-id");
            var diagramOptions = getDiagramOptions();

            var animateStep = function() {
                var hasData = false;

                $.each(appGraphs[diagramId].seriesData, function(i, d) {
                    if (d.length < diagramOptions.series[i].data.length) {
                        hasData = true;
                        d.push(diagramOptions.series[i].data[d.length]);
                    }
                });

                if (hasData) {
                    appGraphs[diagramId].graph.update();
                    setTimeout(animateStep, 0);
                }
                else {
                    $playBtn.removeClass("disabled");
                }
            };

            if (diagramOptions !== null && appGraphs[diagramId] !== undefined) {
                // disable play button
                $playBtn.addClass("disabled");

                $.each(appGraphs[diagramId].seriesData, function(i, d) {
                    d.length = 0; // clear the original array in case it's referenced elsewhere
                });
                animateStep();
            }

            return false;
        });

        $(window).on("resize", function() {
            $(".diagram-graph").each(function(i, d) {
                resizeGraph($(this));
            });
        });
    }

    function resizeGraph($diagramGraph) {

        var graphObj = appGraphs[$diagramGraph.parent().parent().attr("data-diagram-id")];
        if (graphObj && graphObj.hasOwnProperty("graph")) {
            graphObj.graph.configure({
                width: ($diagramGraph.parent().width()
                            - $diagramGraph.siblings(".diagram-legend").outerWidth()
                            - $diagramGraph.siblings(".diagram-axis-y").outerWidth() - 10)
            });
            graphObj.graph.render();
        }
    }
    $(document).ready(function() {
        setupEventTriggers();
        
    });
        

}
