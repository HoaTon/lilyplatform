var task_vis = function (level, data) {
    if (level.uuid == '83177e1d-9da7-4e8b-9b49-2b8739333052') {
        this["lv_task_vis"](data);
    } else if ( (level.uuid == 'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9') || (level.uuid == '80b2246e-6e3e-4dcf-ad41-c3ab0dd948c4')) {
        data['level_id']=level.id;
        this["parkinson_task_vis"](data);
    }
}

var play_vis = function (level, data) {
    if (level.uuid == '83177e1d-9da7-4e8b-9b49-2b8739333052') {
        this["lv_play_vis"](data);
    } else if ( (level.uuid == 'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9') || (level.uuid == '80b2246e-6e3e-4dcf-ad41-c3ab0dd948c4')) {
        this["parkinson_play_vis"](data);
    }
}
