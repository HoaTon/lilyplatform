var LilyDataVisualization = function() {

    // ===================== Start of Template ========================
    var _diagramWrapperTemplate = '\
            <div class="well diagram-wrapper">\
                <div class="row">\
                    <h4 class="span4"></h4>\
                    <div class="btn-group pull-right">\
                        <a class="btn play-diagram" href="#"><i class="icon-play"></i> Play</a>\
                    </div>\
                </div>\
                <div class="diagram-graph-wrapper">\
                    <div class="diagram-axis-y"></div>\
                    <div class="diagram-graph"></div>\
                    <div class="diagram-axis-x"></div>\
                    <div class="diagram-legend"></div>\
                </div>\
            </div>';
    // ---------------------- End of Template -------------------------


    // ============= Start of vis app core functions ==================

    // config
    var diagramConfig = {
        "/api/v1/level/1/": {
            "id": 1,
            "title": "Task synchronization",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "easy (left)", "color": "#0cc", "source": "grass3", "dataX": "xleft", "dataY": "yleft" },
                { "name": "easy (right)", "color": "#0cc", "source": "grass3", "dataX": "xright", "dataY": "yright" },
                { "name": "medium (left)", "color": "#c0c", "source": "grass5", "dataX": "xleft", "dataY": "yleft" },
                { "name": "medium (right)", "color": "#c0c", "source": "grass5", "dataX": "xright", "dataY": "yright" },
                { "name": "hard (left)", "color": "#cc0", "source": "grass7", "dataX": "xleft", "dataY": "yleft" },
                { "name": "hard (right)", "color": "#cc0", "source": "grass7", "dataX": "xright", "dataY": "yright" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/2/": {
            "id": 2,
            "title": "Micrographia",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "movement", "color": "#0cc", "source": "waterPosition", "dataX": "x", "dataY": "y" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/3/": {
            "id": 3,
            "title": "Short time memory(number)",
            "renderer": "line",
            "interpolation": "step-after",
            "series": [
                { "name": "easy", "color": "#0cc", "source": "animal3", "dataX": "time", "dataY": "serial" },
                { "name": "medium", "color": "#c0c", "source": "animal4", "dataX": "time", "dataY": "serial" },
                { "name": "hard", "color": "#cc0", "source": "animal5", "dataX": "time", "dataY": "serial" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Horizontal axis shows the time in seconds, while vertial axis shows the animals by their labeled number.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        },
        "/api/v1/level/4/": {
            "id": 4,
            "title": "Short time memory(2D path)",
            "renderer": "line",
            "interpolation": "linear",
            "series": [
                { "name": "easy (actual)", "color": "#0cc", "source": "crop0position", "dataX": "x", "dataY": "y" },
                { "name": "easy (correct)", "color": "#0ea", "source": "crop0correct", "dataX": "serialx", "dataY": "serialy" },
                { "name": "medium (actual)", "color": "#c0c", "source": "crop1position", "dataX": "x", "dataY": "y" },
                { "name": "medium (correct)", "color": "#a0e", "source": "crop1correct", "dataX": "serialx", "dataY": "serialy" },
                { "name": "hard (actual)", "color": "#cc0", "source": "crop2position", "dataX": "x", "dataY": "y" },
                { "name": "hard (correct)", "color": "#ea0", "source": "crop2correct", "dataX": "serialx", "dataY": "serialy" }
            ],
            "description": '<div><span class="label label-info">Note</span></div>' +
                '<ul><li>Both axes are in pixels on the iPad.</li>' +
                '<li>Hover over the legend to hightlight a line.</li>' +
                '<li>Click on the legend to show a single line.</li>' +
                '<li>Click the ✔ to toggle a line.</li></ul>'
        }
    };

    // runtime data
    var appDataId = 0;
    var appData = null;
    var appGraphs = {};
    var minX = minY = 5000;
    var maxX = maxY = 0;

    function loadAppData() {
        $.getJSON(
            "/api/v1/patientlevelrecord/" + appDataId + "/?format=json",
            function(data) {

                // parse json string to object
                try {
                    data.data = JSON.parse(data.data);
                }
                catch(error) {
                }
                appData = data;

                // pre-process data
                if (diagramConfig.hasOwnProperty(appData["level"]) && diagramConfig[appData["level"]].hasOwnProperty("preprocess")) {
                    diagramConfig[appData["level"]]["preprocess"](appData.data);
                }

                // generate diagrams
                var diagramOptions = getDiagramOptions();
                addDiagram(diagramOptions);

                // show description
                if (diagramOptions !== null) {
                    $("#diagram-description").html(diagramConfig[appData["level"]]["description"]);
                }
            }
        );
    }

    function getDiagramOptions() {
        var levelUri = appData["level"];
        if (!diagramConfig.hasOwnProperty(levelUri)) {
            console.log("The level is unknown!");
            return null;
        }
        var dc = diagramConfig[levelUri];

        var diagramOptions = {
            id: dc.id,
            title: dc.title,
            renderer: dc.renderer,
            interpolation: dc.interpolation,
            series: []
        };

        $.each (dc.series, function(i, d) {
            var seriesObj = {
                name: d.name,
                color: d.color,
                data: []
            };

            // pop up the data array
            $.each(appData.data[d.source], function(ii, dd) {
                seriesObj.data.push({
                    "x": +dd[d.dataX],
                    "y": +dd[d.dataY]
                });

                var x = parseInt(dd[d.dataX]);
                var y = parseInt(dd[d.dataY]);
                if(x < minX)
                    minX = x;
                if(x > maxX)
                    maxX = x;
                if(y < minY)
                    minY = y;
                if(y > maxY)
                    maxY = y;
            });
            diagramOptions.series.push(seriesObj);
        });
        return diagramOptions;
    }

    // crate (append) a new diagram if there is no one having the same data-diagram-id
    // or remove the existing one, then create one at the same place
    function addDiagram(options) {

        var defaultOptions = {
            id: "0",
            title: "Diagram",
            renderer: "line",
            interpolation: "linear",
            series: []
        };

        if (typeof options == "object") {
            options = $.extend(defaultOptions, options);
        } else {
            options = defaultOptions;
        }

        var diagramId = "diagram" + options.id;
        var $diagramWrapper = $(_diagramWrapperTemplate).attr("data-diagram-id", options.id);
        var $existingDiagramWrapper = $(".diagram-wrapper[data-diagram-id=" + options.id +"]");

        if ($existingDiagramWrapper.length > 0) {
            // replace the first existing diagram with new one (assuming that it's the only one)
            $existingDiagramWrapper.first().before($diagramWrapper).remove();
        }
        else {
            // append a new one
            $("#diagrams-container").append($diagramWrapper);
        }

        $diagramWrapper.find("h4").text(options.title);
        var $diagramGraph = $diagramWrapper.find(".diagram-graph");

        // build the diagram
        var graph = new Rickshaw.Graph({
            element: $diagramGraph[0],
            renderer: options.renderer,
            series: options.series,
            rangeX: [minX, maxX],
            rangeY: [minY, maxY],
            //min: "auto",
            interpolation: options.interpolation
        });

        // add axis
        var xAxes = new Rickshaw.Graph.Axis.X({
            graph: graph,
            orientation: 'bottom',
            element: $diagramWrapper.find(".diagram-axis-x")[0]
        });
        var yAxis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            orientation: 'left',
            element: $diagramWrapper.find(".diagram-axis-y")[0]
        });

        if (appData["level"] === "/api/v1/level/3/") {
            // show hover details for 3rd level
            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                xFormatter: function(x) { return x + " seconds" },
                formatter: function(series, x, y) {
                    return '<span>Animal Label:</span> ' + (y || 'None') + '<br>' +
                        '<span>Time:</span> ' + x + ' seconds';
                }
            });
        }

        // add legend
        var legend = new Rickshaw.Graph.Legend({
            graph: graph,
            element: $diagramWrapper.find(".diagram-legend")[0]
        });
        var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
            graph: graph,
            legend: legend
        });
        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
            graph: graph,
            legend: legend
        });

        // series data is saved as 2nd level reference, with may be reordered by rickshaw during the legend hover behavior
        appGraphs[options.id] = {
            graph: graph,
            seriesData: $.map(options.series, function(d, i) { return [d.data]; })
        };

        graph.render();
        resizeGraph($diagramGraph);
    }

    function setupEventTriggers() {

        $("#diagrams-container").on("click", ".play-diagram", function() {
            var $playBtn = $(this);
            if ($playBtn.hasClass("disabled"))
                return false;

            var diagramId = $(this).parents(".diagram-wrapper").attr("data-diagram-id");
            var diagramOptions = getDiagramOptions();

            var animateStep = function() {
                var hasData = false;

                $.each(appGraphs[diagramId].seriesData, function(i, d) {
                    if (d.length < diagramOptions.series[i].data.length) {
                        hasData = true;
                        d.push(diagramOptions.series[i].data[d.length]);
                    }
                });

                if (hasData) {
                    appGraphs[diagramId].graph.update();
                    setTimeout(animateStep, 0);
                }
                else {
                    $playBtn.removeClass("disabled");
                }
            };

            if (diagramOptions !== null && appGraphs[diagramId] !== undefined) {
                // disable play button
                $playBtn.addClass("disabled");

                $.each(appGraphs[diagramId].seriesData, function(i, d) {
                    d.length = 0; // clear the original array in case it's referenced elsewhere
                });
                animateStep();
            }

            return false;
        });

        $(window).on("resize", function() {
            $(".diagram-graph").each(function(i, d) {
                resizeGraph($(this));
            });
        });
    }

    function resizeGraph($diagramGraph) {

        var graphObj = appGraphs[$diagramGraph.parent().parent().attr("data-diagram-id")];
        if (graphObj && graphObj.hasOwnProperty("graph")) {
            graphObj.graph.configure({
                width: ($diagramGraph.parent().width()
                            - $diagramGraph.siblings(".diagram-legend").outerWidth()
                            - $diagramGraph.siblings(".diagram-axis-y").outerWidth() - 10)
            });
            graphObj.graph.render();
        }
    }

    $(document).ready(function() {
        setupEventTriggers();
    });

    function init(id) {
        appDataId = id;
        loadAppData();
    }

    return {
        init: init
    }
}();