
var lv_task_vis= function(data){
    var template = '\
        <div class="col-lg-12">\
            <h4>Overall performance</h4>\
            <span class="text-muted">Overall performance over time</span>\
            <div id="chart1">\
                <svg></svg>\
            </div>\
            <hr/>\
            <h4>Prospective memory</h4>\
            <span class="text-muted">Prospective memory over time</span>\
            <div id="chart2">\
                <svg></svg>\
            </div>\
            <hr/>\
            <h4>Correlations</h4>\
            <span class="text-muted">Correlations between absorption, speed, and prospective memory</span>\
            <div id="chart3"></div>\
        </div>\
        <!-- /.col-lg-8 (nested) -->\
    ';
    var graphBody = $('#task-visualization-body');
    graphBody.html(template);

    selectors = ['#chart1 svg','#chart2 svg','#chart3'];
    chart_allsession(selectors,data);
}

var lv_play_vis= function(data){
    var template='\
    <div class="col-lg-12">\
        <div class="row">\
            <div class="col-lg-12">\
                <h4>Session Performance</h4>\
                <span class="text-muted">The session prospective memory, absorption, and speed</span>\
                <div id="overall-chart">\
                    <svg></svg>\
                </div>\
                <!--<canvas id="correlation_chart" width="400" height="350">\
                </canvas>-->\
            </div>\
        </div>\
        <hr/>\
        <div class="row">\
            <div class="col-lg-12">\
                <h4>Prospective memory</h4>\
                <span class="text-muted">Prospective memory evaluated based on the 3 PM tasks</span>\
                <div id="memoryblock-chart">\
                    <svg></svg>\
                </div>\
            </div>\
        </div>\
        <hr/>\
        <div class="row">\
            <div class="col-md-12">\
                <h4>Comparison</h4>\
                <div class="comparision-chart col-md-4">\
                    <div id="comparision-chart1" style=""><svg></svg></div>\
                    <h4>Prospective Memory Score</h4>\
                    <span class="text-muted">My prospective memory in this session compared with all the other game sessions</span>\
                </div>\
                <div class="comparision-chart col-md-4">\
                    <div id="comparision-chart2" style=""><svg></svg></div>\
                    <h4>Speed Score</h4>\
                    <span class="text-muted">My speed in this session compared with all the other game sessions</span>\
                </div>\
                <div class="comparision-chart col-md-4">\
                    <div id="comparision-chart3" style=""><svg></svg></div>\
                    <h4>Absorption Score</h4>\
                    <span class="text-muted">My absorption in this session compared with all the other game sessions</span>\
                 </div>\
            </div>\
        </div>\
        <hr/>\
        <span class="label label-info">Note</span>\
        <ul>\
            <li>Speed: The overall speed of playing. Higher score indicates faster.</li>\
            <li>Prospective Memory:  The ability of prospective memory. Higher score indicates better prospective memory performance.</li>\
            <li>Absorption:  The involvement in the ongoing tasks. Higher score indicates more resource spent on the two main tasks.</li>\
        </ul>\
    </div><!-- /.col-lg-12 -->\
    ';
    var graphBody = $('#play-visualization-body');
    graphBody.html(template);

    selectors = ['#overall-chart svg', '#memoryblock-chart svg'];
    chart_play(selectors, data);

    selectors = ['#comparision-chart1 svg', '#comparision-chart2 svg', '#comparision-chart3 svg'];
    chart_compare(selectors,data);
}

function chart_play(selectors,data){

    var pm1_val = parseInt(data["scores"].pm_memory_scores.pm_flip_memory_score);
    var pm1_text = "PM1: " + pm1_val;
    var pm2_val = parseInt(data["scores"].pm_memory_scores.pm_fairy_memory_score);
    var pm2_text = "PM2: " + pm2_val;
    var pm3_val = parseInt(data["scores"].pm_memory_scores.pm_fisherman_memory_score);
    var pm3_text = "PM3: " + pm3_val;

    var chart_data = [
      {
        "key": "session performance",
        "color": "#d62728",
        "values": [
          {
            "label" : "Absorption score" ,
            "value" : data["scores"].user_total_collect_score
          } ,
          {
            "label" : "Speed score" ,
            "value" : data["scores"].user_total_time_score
          } ,
          {
            "label" : "Memory score" ,
            "value" : data["scores"].pm_memory_scores.pm_memory_score
          }
        ]
      }
    ]

    nv.addGraph(function() {
      var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .margin({top: 100, right: 20, bottom: 50, left: 125})
          .height(240)
          .showValues(true)
          .tooltips(false)
          .showLegend(false)
          .showControls(false);

      chart.yAxis
          .tickFormat(d3.format('d'));

      chart.forceY([0,100])

      d3.select(selectors[0])
          .datum(chart_data)
        .transition().duration(500)
          .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });

    var pi = Math.PI;
    var vis = d3.select(selectors[1]);

    var dy = -5
    var x = 60
    var opacity = 0.8
    var colors = ["#6baed6", "#fd8d3c", "#74c476", "#ff8c00"]
    var background = "#D3D3D3";

    var arc = d3.svg.arc()
        .innerRadius(90)
        .outerRadius(120)
        .startAngle(0);

    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path1")
        .attr("d", arc.startAngle(0), arc.endAngle((1/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        .attr("d", arc.startAngle(0), arc.endAngle(((1/3) * 2*pi-0.1)*pm1_val*0.01))
        .attr("fill", colors[0])
        .attr("data-legend","PM1: Time-based (after 30 seconds)")
        .attr("data-legend-pos", 1);;

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path1")
        .text(pm1_text);


    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path2")
        .attr("d", arc.startAngle((1/3) * 2*pi), arc.endAngle((2/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        .attr("d", arc.startAngle((1/3) * 2*pi), arc.endAngle((1/3) * 2*pi + ((1/3) * 2*pi-0.1)*pm2_val*0.01))
        .attr("fill", colors[1])
        .attr("data-legend","PM2: Time-based (at a particular time)")
        .attr("data-legend-pos", "2");

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path2")
        .text(pm2_text);


    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path3")
        .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((3/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        // .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((9/10) * 2*pi)) //pm3_val
        .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((2/3) * 2*pi + ((1/3) * 2*pi-0.1)*pm3_val*0.01))
        .attr("fill", colors[2])
        .attr("data-legend","PM3: Event-based (at the appearance of the boat)")
        .attr("data-legend-pos", "3");;

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path3")
        .text(pm3_text);

    /*
    var legend = vis.append("svg")
       .attr("class", "legend")
       .attr("width", 180)
       .attr("height", 180)
       .selectAll("g")
       .data(color.domain().slice().reverse())
       .enter().append("g")
       .attr("transform", function(d, i) { return "translate(100," + i * 20 + ")"; })
    */


    legend = vis.append("g")
        .attr("class","legend")
        .attr("transform","translate(250,30)")
        .style("font-size","12px")
        .call(d3.legend)
}

function chart_compare(selectors,data){
    var time_scores = data["all_scores"].user_time_scores;
    var collect_scores = data["all_scores"].user_collect_scores;
    var memory_scores = data["all_scores"].user_memory_scores;

    var build_chart = function(values, svg, w, h, score) {
      // A formatter for counts.
      var formatCount = d3.format(",.0f");

      var margin = {top: 10, right: 30, bottom: 30, left: 30},
          width = w - margin.left - margin.right,
          height = h - margin.top - margin.bottom;

      var x = d3.scale.linear()
          .domain([0, 100])
          .range([0, width]);

      // Generate a histogram using twenty uniformly-spaced bins.
      var data = d3.layout.histogram()
          .bins(x.ticks(20))
          (values);

      var y = d3.scale.linear()
          .domain([0, d3.max(data, function(d) { return d.y; })])
          .range([height, 0]);

      var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom");

      var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left")
          .tickFormat(d3.format('d'));

      var svg = d3.select(svg)
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      var bar = svg.selectAll(".bar")
          .data(data)
        .enter().append("g")
          //.attr("class", "bar")
          .attr("class", function(d) { return d.x <= score && score <= d.x+ 5 ? "special" : "bar"; })
          .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

      bar.append("rect")
          .attr("x", 1)
          .attr("width", x(data[0].dx) - 1)
          .attr("height", function(d) { return height - y(d.y); });

      bar.append("text")
          .attr("dy", ".75em")
          .attr("y", 6)
          .attr("x", x(data[0].dx) / 2)
          .attr("text-anchor", "middle")
          .text(function(d) { return formatCount(d.y); });

      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);

      svg.append("g")
          .attr("class", "y axis")
          // .attr("transform", "translate(0," + height + ")")
          .attr("transform", "translate(0, 0)")
          .call(yAxis);
    }

    build_chart(memory_scores, selectors[0], 290, 200, data["scores"].pm_memory_scores.pm_memory_score);
    build_chart(time_scores, selectors[1], 290, 200, data["scores"].user_total_time_score);
    build_chart(collect_scores, selectors[2], 290, 200, data["scores"].user_total_collect_score);
}

function chart_allsession(selectors,data){

  // data retriving
  var time_scores = data["user_time_scores"];
  var collect_scores = data["user_collect_scores"];
  var memory_scores = data["user_memory_scores"];
  var pm_flip_memory_scores = data["pm_flip_memory_scores"];
  var pm_fairy_memory_scores = data["pm_fairy_memory_scores"];
  var pm_fisherman_memory_scores = data["pm_fisherman_memory_scores"];

  //dummy data
  //var time_scores = [30, 50]
  //var collect_scores = [50, 50]
  //var memory_scores = [76, 25]
  //var pm_flip_memory_scores = [76, 0]
  //var pm_fairy_memory_scores = [76, 0]
  //var pm_fisherman_memory_scores = [76, 76]
  //enddummy

  var memory_score_series = [];
  var time_score_series = [];
  var negative_time_score_series = [];
  var collect_score_series = [];
  var negative_collect_score_series = [];

  var pm_flip_memory_series = []
  var pm_fairy_memory_series = []
  var pm_fisherman_memory_series = []

  for (var i = 0; i < pm_flip_memory_scores.length; i++) {
    pm_flip_memory_series.push({x: i+1, y: pm_flip_memory_scores[i]});
    pm_fairy_memory_series.push({x: i+1, y: pm_fairy_memory_scores[i]});
    pm_fisherman_memory_series.push({x: i+1, y: pm_fisherman_memory_scores[i]});
  }

  for (var i = 0; i < time_scores.length; i++) {
    memory_score_series.push({x: i+1, y: memory_scores[i]});
    time_score_series.push({x: i+1, y: time_scores[i]});
    negative_time_score_series.push({x: i+1, y: -time_scores[i]});
    collect_score_series.push({x: i+1, y: collect_scores[i]});
    negative_collect_score_series.push({x: i+1, y: -collect_scores[i]});
  }

  // ----------------------------------
  var chart1_data = function() {
    return [
      {
        values: memory_score_series,
        key: 'Prospective Memory',
        color: '#1F77B4'
      },
      {
        values: time_score_series,
        key: 'Speed',
        color: '#ff7f0e'
      },
      {
        values: collect_score_series,
        key: 'Absorption',
        color: '#2ca02c'
      }
    ];
  }


  nv.addGraph(function() {
    var chart = nv.models.lineChart()
      .useInteractiveGuideline(true)
      .forceY([0,100])
      ;

    chart.xAxis
      .axisLabel('Session Index')
      .tickFormat(d3.format(',d'))
      ;

    chart.yAxis
      .axisLabel('Voltage (v)')
      .tickFormat(d3.format('d'))
      ;

    d3.select(selectors[0])
      .datum(chart1_data())
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
  });

  // ----------------------------------

  var layers = [
    {
      "key": "Flip",
      "values": pm_flip_memory_series,
      "color": "#d62728"
    },
    // {
    //   "key": "Prospective Memory",
    //   "values": leftover_memory_score_series,
    //   "color": "#D3D3D3"
    // },
    {
      "key": "Fairy",
      "values": pm_fairy_memory_series,
      "color": "#9467bd"
    },
    // {
    //   "key": "Speed",
    //   "values": leftover_time_score_series,
    //   "color": "#D3D3D3"
    // },
    {
      "key": "Fisherman",
      "values": pm_fisherman_memory_series,
      "color": "#8c564b"
    },
    // {
    //   "key": "Absorption",
    //   "values": leftover_collect_score_series,
    //   "color": "#D3D3D3"
    // },
  ];


  nv.addGraph(function() {
    var chart = nv.models.lineChart()
      .forceY([0,100])
      .useInteractiveGuideline(true)
      ;

    chart.xAxis
      .axisLabel('Session Index')
      .tickFormat(d3.format(',d'))
      ;

    chart.yAxis
      .axisLabel('Voltage (v)')
      .tickFormat(d3.format('d'))
      ;

    d3.select(selectors[1])
      .datum(layers)
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
  });

    scatterPlot3d( d3.select(selectors[2]), memory_score_series, time_score_series, collect_score_series);
    x3dom.reload();
}

