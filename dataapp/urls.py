from __future__ import absolute_import
from django.conf.urls import patterns, include, url
from .views import *
from .routers import router as api_router
from .blogpost_urls import user_urls, post_urls, photo_urls, blogpost_urls
from .game_urls import gameplatform_urls


urlpatterns = patterns('',
    url(r'^$', view=home_view, name='home'),
    url(r'^players/(?P<player_pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/$', view=player_game_view, name='player_game_view'),

    url(r'^profile/$', view=profile, name='profile-page'),
    url(r'^blogpost/$', view=blogpost, name='blogpost-page'),

    # APIs
    url(r'^apidoc/', view=apidoc, name='apidoc'),
    url(r'^apireference/', view=apireference, name='apireference'),
    url(r'^api/', include(gameplatform_urls)),
    url(r'^rest-auth/', include('rest_auth.urls')),

    # samples
    url(r'^profile-api/', include(api_router.urls)),
    url(r'^upload/', view=upload_image, name='photo-upload'),
    url(r'^blogpost-api/', include(blogpost_urls)),

    # statistics
    url(r'^statistics/', view=statistics, name='statistics'),
    url(r'^user_statistics/$', view=user_statistics, name='user_statistics'),
    url(r'^user_statistics/user_statistics_table/$', view=user_statistics_table, name='user_statistics_table'),
    url(r'^game_statistics/$', view=game_statistics, name='game_statistics'),
    url(r'^game_statistics/game_statistics_table/$', view=game_statistics_table, name='game_statistics_table'),

    url(r'^game_level_statistics/$', view=game_level_statistics, name='game_level_statistics'),
    url(r'^game_level_statistics/game_level_statistics_table/$', view=game_level_statistics_table, name='game_level_statistics_table'),

    # analysis
    url(r'^analysis/', view=analysis, name='analysis'),

    url(r'^crossdomain.xml', view=crossdomain, name='crossdomain'),
)
