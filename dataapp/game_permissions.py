from rest_framework.permissions import BasePermission
from django.shortcuts import get_object_or_404

from .models import Player


class CanAccessUserPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class CanAccessPlayerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        # only the doctor of the player or the player-self to access
        isPlayerSelf = (obj.user == request.user)
        isDoctorofPlayer = (request.user.userprofile.user_type=='staff') and (request.user.staff_profile in obj.doctors.all())
        if isPlayerSelf or isDoctorofPlayer:
            canAccess = True
        else:
            canAccess = False

        return canAccess


class CanAccessPlanPermission(BasePermission):
    def has_permission(self, request, view):
        isMyPlan = view.kwargs.get('pk', '') == str(request.user.player_profile.pk)

        isDoctorofPlayer = False
        player_id = view.kwargs.get('pk', '')
        if player_id:
            player = get_object_or_404(Player, pk=player_id)
            isDoctorofPlayer = (request.user.userprofile.user_type=='staff') and (request.user.staff_profile in player.doctors.all())

        if isMyPlan or isDoctorofPlayer:
            canAccess = True
        else:
            canAccess = False
        return canAccess


class CanAccessPlayPermission(BasePermission):
    # def has_permission(self, request, view):
    #     # TODO: ensure game_level.game == plan.game
    #     return True

    def has_object_permission(self, request, view, obj):
        # only the doctor of the player or the player-self to access
        # obj is current player
        isPlayerSelf = (obj.player.user == request.user)
        isDoctorofPlayer = (request.user in obj.player.doctors.all())
        if isPlayerSelf or isDoctorofPlayer:
            canAccess = True
        else:
            canAccess = False

        return canAccess


class CanAccessDataRecordPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        # only the doctor of the player or the player-self to access
        # obj is current player
        isPlayerSelf = (obj.game_play.player.user == request.user)
        isDoctorofPlayer = (request.user in obj.game_play.player.doctors.all())
        if isPlayerSelf or isDoctorofPlayer:
            canAccess = True
        else:
            canAccess = False

        return canAccess
