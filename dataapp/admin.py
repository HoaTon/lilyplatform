from django.contrib import admin
from .models import *
from import_export.admin import *
from import_export.widgets import *
from import_export import fields, resources

admin.site.register(Profile)
admin.site.register(ProfilePhoto)

admin.site.register(Post)
admin.site.register(Photo)

admin.site.register(UserProfile)
admin.site.register(Organization)
admin.site.register(Staff)
admin.site.register(StaffRelationship)
admin.site.register(Player)
admin.site.register(GameSeries)
admin.site.register(UserGameFeedback)
admin.site.register(PlayerAvatar)
#admin.site.register(PlayerGameDataRecord)

admin.site.register(Topic)
admin.site.register(Thread)
admin.site.register(Comment)

class PlayerGameDataRecordResource(resources.ModelResource):
    #player = fields.Field()
    #game = fields.Field()
    game_play = fields.Field(column_name='game_play', attribute='game_play', widget=ForeignKeyWidget(PlayerGamePlay, 'uuid'))
    class Meta:
        model = PlayerGameDataRecord
        #exclude = ('id', 'game_play', 'data_type', 'data_schema')
        exclude = ('id',)
        import_id_fields = ['data']

    '''
    def dehydrate_player(self, playergamedatarecord):
        return '%s' % playergamedatarecord.game_play.player.user.username

    def dehydrate_game(self, playergamedatarecord):
        return '%s: %s' % (playergamedatarecord.game_play.game_level.game.name, playergamedatarecord.game_play.game_level.name)
    '''
class PlayerGamePlayResource(resources.ModelResource):

    class Meta:
        model = PlayerGamePlay
        exclude = ('id', 'score')
        import_id_fields = ['uuid',]


class PlayerGameDataRecordAdmin(ImportExportModelAdmin):
    #readonly_fields = ('uuid',)
    resource_class = PlayerGameDataRecordResource
admin.site.register(PlayerGameDataRecord, PlayerGameDataRecordAdmin)

class GameAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(Game, GameAdmin)


class GameLevelAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(GameLevel, GameLevelAdmin)


class PlayerGamePlayAdmin(ImportExportModelAdmin):
    readonly_fields = ('uuid',)
    resource_class = PlayerGamePlayResource
admin.site.register(PlayerGamePlay, PlayerGamePlayAdmin)

class PlanAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(Plan, PlanAdmin)

class UserResource(resources.ModelResource):
    class Meta:
        model = User

class UserResource(resources.ModelResource):
    class Meta:
        model = User
        #fields = ('first_name', 'last_name', 'email')

class UserAdmin(ImportExportModelAdmin):
    resource_class = UserResource
    pass

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
