import math, json
import pdb
from datetime import datetime
from dataapp.models import PlayerGamePlay, PlayerGameDataRecord

def task_vis(player_id, gamelevel_uuid):
    """
        player_id is the primary key in Player Table
        obtain the data needed for the virtualization from the db by yourself
    """
    all_plays = PlayerGamePlay.objects.filter(player__pk=player_id).filter(game_level__uuid=gamelevel_uuid)
    ret_data = get_scores(all_plays)

    return ret_data


def play_vis(play_id):
    """
        play_id is the primary key in PlayerGamePlay Table
        obtain the data needed for the virtualization from the db by yourself
    """

    records = PlayerGameDataRecord.objects.filter(game_play__pk=play_id)
    scores = get_user_metrics(RecordEventListAdapter(records))

    play = PlayerGamePlay.objects.get(pk=play_id)
    all_plays = PlayerGamePlay.objects.filter(player=play.player).filter(game_level=play.game_level)
    user_score = get_scores(all_plays)

    ret_data = {"scores"    : scores,
                "all_scores": user_score}

    return ret_data

class RecordEventListAdapter:
    """play_id
    Adapter to convert list of Record data type to list of Event data type.
    Refer to RecordEventAdapter for details field mapping.
    TODO
    """
    def __init__(self, records):
        self.__records = records

    def order_by(self, field_name):
        if field_name == "-event_time":                 #TODO: quick hack. supposed to sorted by event_time field,
            self.__records.order_by("-created")         #which is not available in new database
        return self

    def get(self, event_name="null"):
        record = self.__records.get(data_schema=event_name)
        return RecordEventAdapter(record)

    def filter(self, event_name="null"):
        ret = self.__records.filter(data_schema=event_name)
        return RecordEventListAdapter(ret)

    def first(self):
        return RecordEventAdapter(self.__records[0])

    def count(self):
        return self.__records.count()


class RecordEventAdapter:
    """
        Adapter to convert Record data type to Event data type
        Map :   event_name => record.data_schema
                event_type => record.data_type
                event_time => record.created
                {'data'     :data,
                'event_time':event_time,
                'extra_data':extra_data} => record.data

    """

    def __init__(self, record):
        self.__record = record
        self.event_type = record.data_type
        self.insert_time = record.created
        data_json = json.loads(record.data)
        self.data = data_json['data']
        self.event_time = datetime.strptime(data_json['event_time'], "%Y-%m-%d %H:%M:%S")
        self.extra_data = data_json['extra_data']


def get_scores(gameplays):
    user_memory_scores = []
    pm_flip_memory_scores = []
    pm_fairy_memory_scores = []
    pm_fisherman_memory_scores = []
    user_time_scores = []
    user_collect_scores = []
    for play in gameplays:
        records = PlayerGameDataRecord.objects.filter(game_play=play)
        events = RecordEventListAdapter(records)
        try:
            scores = get_user_metrics(events)
            user_memory_scores.append(scores['pm_memory_scores']['pm_memory_score'])
            pm_flip_memory_scores.append(scores['pm_memory_scores']['pm_flip_memory_score'])
            pm_fairy_memory_scores.append(scores['pm_memory_scores']['pm_fairy_memory_score'])
            pm_fisherman_memory_scores.append(scores['pm_memory_scores']['pm_fisherman_memory_score'])
            user_time_scores.append(scores['user_total_time_score'])
            user_collect_scores.append(scores['user_total_collect_score'])
        except Exception as ex:
            print(ex.message)
            pass
    user_scores = {
        "user_memory_scores": user_memory_scores,
        "pm_flip_memory_scores": pm_flip_memory_scores,
        "pm_fairy_memory_scores": pm_fairy_memory_scores,
        "pm_fisherman_memory_scores": pm_fisherman_memory_scores,
        "user_time_scores": user_time_scores,
        "user_collect_scores": user_collect_scores
    }
    return user_scores


''' utils functions '''
baseline_total_time = 60.0 * 5

baseline_fish_count = 6.0
baseline_pedal_count = 6.0

pm_flip_expected_time = 30.0
pm_fairy_expected_time = 30.0
pm_fisherman_expected_time = 30.0

algorithm = 1

def get_user_metrics(events):
    # import pdb; pdb.set_trace()
    start_game_event = events.get(event_name='Start Game')
#   end_game_event = events.filter(event_name='End Game').order_by('-event_time')[0]
    end_game_event = events.filter(event_name='End Game').order_by('-event_time').first()
    collect_fish_event = events.filter(event_name='Dried Fish Collection')
    collect_pedal_event = events.filter(event_name='Dried Petal Collection')

    # user total time score
    total_time = end_game_event.event_time - start_game_event.event_time
    user_total_time = total_time.total_seconds()
    user_total_time_score = 100 if user_total_time > baseline_total_time else 100 * (user_total_time / baseline_total_time)

    # use total collection score
    user_total_fish = collect_fish_event.count()
    user_total_pedal = collect_pedal_event.count()
    user_total_fish_score = 100 if user_total_fish > baseline_fish_count else 100 * (user_total_fish / baseline_fish_count)
    user_total_pedal_score = 100 if user_total_pedal > baseline_pedal_count else 100 * (user_total_pedal / baseline_pedal_count)
    user_total_collect_score = (user_total_fish_score + user_total_pedal_score) / 2.0

    pm_flip_memory_score = get_pmtask_memory_performance(events, 'Flip_Task_1_Started', 'Flip_Task_1_Ended', 'Flip_Task_1', pm_flip_expected_time)
    pm_fairy_memory_score = get_pmtask_memory_performance(events, 'Gift_Fairy_Task_1_Started', 'Gift_Fairy_Task_1_Ended', 'Gift_Fairy_Task_1', pm_fairy_expected_time)
    pm_fisherman_memory_score = get_pmtask_memory_performance(events, 'Gift_Fisherman_Task_1_Started', 'Gift_Fisherman_Task_1_Ended', 'Gift_Fisherman_Task_1', pm_fisherman_expected_time)
    pm_memory_score = (pm_flip_memory_score + pm_fairy_memory_score + pm_fisherman_memory_score) / 3.0

    pm_memory_score, pm_fairy_memory_score, pm_fairy_memory_score, pm_fisherman_memory_score, user_total_time_score, user_total_collect_score = map(int, [pm_memory_score, pm_fairy_memory_score, pm_fairy_memory_score, pm_fisherman_memory_score, user_total_time_score, user_total_collect_score])

    scores = {
        "pm_memory_scores": {
            "pm_memory_score": pm_memory_score,
            "pm_flip_memory_score": pm_flip_memory_score,
            "pm_fairy_memory_score": pm_fairy_memory_score,
            "pm_fisherman_memory_score": pm_fisherman_memory_score,
        },
        "user_total_time_score": user_total_time_score,
        "user_total_collect_score": user_total_collect_score
    }
    return scores


def get_memory_performance(begin_time, exp_time, user_act_time, end_time, algorithm=0):
    begin_time, exp_time, user_act_time, end_time = 1.0, 10.0, 8.0, 12.0

    if (begin_time>exp_time) or (begin_time>user_act_time) or (end_time<exp_time) or (end_time<user_act_time):
        print "The input time information is wrong, please the system setting."
        return None

    if algorithm==0:
        score = (1-min(abs(exp_time-user_act_time)/float(exp_time-begin_time), 1)) * 100

    elif algorithm==1:
        sigma=(exp_time-begin_time)/3.291
        score = math.exp(-(user_act_time-exp_time)**2/(2*sigma**2)) * 100

    score = int(score)

    return score


def get_pmtask_memory_performance(events, start_event_name, end_event_name, user_event_name, expected_time):
    pm_start_event = events.get(event_name=start_event_name)
    pm_end_event = events.get(event_name=end_event_name)
    try:
        pm_user_event = events.get(event_name=user_event_name)
        user_act_time = (pm_user_event.event_time - pm_start_event.event_time).total_seconds()
        end_time = (pm_end_event.event_time - pm_start_event.event_time).total_seconds()
        user_pm_score = get_memory_performance(0.0, expected_time, user_act_time, end_time, algorithm)
    except:
        print 'EXCEPTION with [%s-%s-%s]' % (start_event_name, user_event_name, end_event_name)
        user_pm_score = 0
    return user_pm_score



