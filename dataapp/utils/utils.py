from importlib import import_module
from dataapp.models import PlayerGamePlay

gamelevel_function_dict = {
    'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9': 'dataapp.utils.parkinson.task_vis',
    '80b2246e-6e3e-4dcf-ad41-c3ab0dd948c4': 'dataapp.utils.parkinson.task_vis',
    '83177e1d-9da7-4e8b-9b49-2b8739333052': 'dataapp.utils.lv.task_vis',
}


play_function_dict = {
    'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9': 'dataapp.utils.parkinson.play_vis',
    '80b2246e-6e3e-4dcf-ad41-c3ab0dd948c4': 'dataapp.utils.parkinson.play_vis',
    '83177e1d-9da7-4e8b-9b49-2b8739333052': 'dataapp.utils.lv.play_vis',
}


def load_module(module_str):
    module_str, vis_func_str = module_str.rsplit('.', 1)
    module = import_module(module_str)
    vis_func = getattr(module, vis_func_str)
    return vis_func


def all_session_visualize(player_id, gamelevel_uuid):
    vis_func = load_module(gamelevel_function_dict[gamelevel_uuid])

    ret_data = vis_func(player_id, gamelevel_uuid)

    return ret_data


def single_session_visualize(play_id):
    play = PlayerGamePlay.objects.get(pk=play_id)
    gamelevel_uuid = play.game_level.uuid

    vis_func = load_module(play_function_dict[gamelevel_uuid])

    ret_data = vis_func(play_id)

    return ret_data
