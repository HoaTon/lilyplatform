from dataapp.models import *
import math
import json
from collections import defaultdict
import collections
try:
    #print "pre"
    from numpy import *
    #print "post"
except:
    print "error"
    pass

def task_vis(player_id, gamelevel_uuid):
    """
        player_id is the primary key in Player Table
        obtain the data needed for the virtualization from the db by yourself
    """
    ret_data = {'hello': 'task vis'}

    sin = []
    other = []
    cos = []
    for i in range(20):
        sin.append({'x': i, 'y': math.sin(float(i)/10)})
        cos.append({'x': i, 'y': 0.5 * math.cos(float(i)/10)})
        other.append({'x': i, 'y': 0.2 * math.sin(float(i)/10)})

    ret_data = [
        {'values': sin, 'key': 'Memory Accuracy', 'color': '#ff7f0e'},
        {'values': cos, 'key': 'Speed of cognition', 'color': '#2ca02c'},
        {'values': other, 'key': 'Game completion'}]
    #above is for example one
    ###################################################################################


    #gamePlay = PlayerGamePlay.objects.filter(player__pk=player_id).filter(game_level__uuid=gamelevel_uuid)
    gamePlay = PlayerGamePlay.objects.filter(player__pk=player_id)

    


    #plays = list(PatientLevelRecord.objects.filter(gameplay =gamePlay)[:100])  # .order_by('-id')[:100]
    plays = list(PlayerGameDataRecord.objects.filter(game_play =gamePlay)[:100])   # .order_by('-id')[:100]
    
    # if plays.count() > 100:
    #     plays = plays[:100]
    #     is_all_plays = 0
    # else:
    #     is_all_plays = 1
    games = Game.objects.all() # not use

    ############# check up to here
    d1 = {
        "id": 1,
        "min": 0,
        "series": [
            {"name": "avg (easy)", "color": "#0cc", "data": []},
            {"name": "avg (medium)", "color": "#c0c", "data": []},
            {"name": "avg (hard)", "color": "#cc0", "data": []},
        ]
    }
    d2 = {
        "id": 2,
        "series": [
            {"name": "Diameter trend", "color": "#0cc", "data": []},
        ]
    }
    d3 = {
        "id": 3,
        "min": 0,
        "series": [
            {"name": "success rate (easy)", "color": "#0cc", "data": []},
            {"name": "success rate (medium)", "color": "#c0c", "data": []},
            {"name": "success rate (hard)", "color": "#cc0", "data": []},
        ]
    }
    d4 = {
        "id": 4,
        "min": 0,
        "series": [
            {"name": "success rate (easy)", "color": "#0cc", "data": []},
            {"name": "success rate (medium)", "color": "#c0c", "data": []},
            {"name": "success rate (hard)", "color": "#cc0", "data": []},
        ]
    }

    
    play_extra_dict = {}
    print "len of plays: "
    print len(plays)
    for play in plays:

        level_measures = evaluate_play(play.id)['level_measures']
        #####################################
        
        play_info = {
            'id': play.id,
            'created': play.created,
            'level_measures': level_measures
        }

        if play.game_play.game_level.id == 1:
            if level_measures:
                try:
                    d1['series'][0]['data'].append({'x': len(d1['series'][0]['data'])+1, 'y': level_measures['grass3']['avg']})
                    d1['series'][1]['data'].append({'x': len(d1['series'][1]['data'])+1, 'y': level_measures['grass5']['avg']})
                    d1['series'][2]['data'].append({'x': len(d1['series'][2]['data'])+1, 'y': level_measures['grass7']['avg']})
                except:
                    pass
        elif play.game_play.game_level.id == 2:
            if level_measures:
                d2['series'][0]['data'].append({'x': len(d2['series'][0]['data'])+1, 'y': level_measures['angle_tan']})

        elif play.game_play.game_level.id == 3:
            if level_measures:
                d3['series'][0]['data'].append({'x': len(d3['series'][0]['data'])+1, 'y': level_measures['animal3']['correct_rate']})
                d3['series'][1]['data'].append({'x': len(d3['series'][1]['data'])+1, 'y': level_measures['animal4']['correct_rate']})
                d3['series'][2]['data'].append({'x': len(d3['series'][2]['data'])+1, 'y': level_measures['animal5']['correct_rate']})

        elif play.game_play.game_level.id == 4:
            if level_measures:
                d4['series'][0]['data'].append({'x': len(d4['series'][0]['data'])+1, 'y': level_measures['crop0use']['correct_rate']})
                d4['series'][1]['data'].append({'x': len(d4['series'][1]['data'])+1, 'y': level_measures['crop1use']['correct_rate']})
                d4['series'][2]['data'].append({'x': len(d4['series'][2]['data'])+1, 'y': level_measures['crop2use']['correct_rate']})

        play_extra_dict[play.id] = play_info

    # populate the measures for each level
    measures = Measure.objects.all()
    print "d1: " + str(d1)
    print "d2: "+str(d2)
    tmp_d_array = [d1, d2, d3, d4]
    for measure in measures:
        if measure.level_id in [1, 2, 3, 4]:
            tmp_d = tmp_d_array[measure.level_id - 1]
            max_x = 0
            for s in tmp_d['series']:
                if s['data'] and s['data'][-1]['x'] > max_x:
                    max_x = s['data'][-1]['x']

            tmp_d['series'].append({
                "name": str(measure.name + ' threshold'),
                "color": "red",
                "data": [
                    {'x': 1, 'y': float(measure.threshold)},
                    {'x': max_x, 'y': float(measure.threshold)},
                ],
            })
    #print "d: "+ str([d1, d2, d3, d4])
    return_object = {
        'd': [d1, d2, d2, d2],
        }

    return return_object

# to use in above method
def evaluate_play(play_id):
    print "play id: "+str(play_id)
    play = PlayerGameDataRecord.objects.filter(id=int(play_id))
    print len(play)
    #print "play: "
    #print play
    ######################################
    #currently stop here
    
    data = json.loads(play[0].data)
    
    alerts = ''
    level_measures = {}
    #print "play_id: "
    #print play[0].game_play.game_level.id
    
    if play[0].game_play.game_level.id == 1:
        measures = Measure.objects.filter(game_level=play[0].game_play.game_level.id)

        syncdata = defaultdict(list)
        grasses = ['grass3', 'grass5', 'grass7']
        for grassname in grasses:
        # grass3, grass5, grass7 = data['grass3'], data['grass5'], data['grass7']
            # import pdb; pdb.set_trace()
            for record in data[grassname]:
                # "xleft":"643","yleft":"25.00003","time":"15.96","xright":"1363","yright":"99"
                point_left = (float(record['xleft']), float(record['yleft']))
                point_right = (float(record['xright']), float(record['yright']))
                line = (1024, 1012)
                point_left_sym = (point_left[0] + 2 * (line[0]-point_left[0]), point_left[1])
                distance = math.sqrt(math.pow(point_right[0]-point_left_sym[0], 2) + math.pow(point_right[1]-point_left_sym[1], 2))
                syncdata[grassname].append(distance)
        for grassname, distances in syncdata.iteritems():
            if distances:
                level_measures[grassname] = {
                    'avg': float(sum(distances)) / len(distances),
                    'min': min(distances),
                    'max': max(distances)
                }

        alerts = []
        for measure in measures:
            for grassname, values in level_measures.iteritems():
                message = diagnose(measure, values['avg'])
                if message:
                    msg = '[%s] %s' % (grassname, message)
                    alerts.append(msg)


    if play[0].game_play.game_level.id == 2:
        low_points = []
        high_points = []
        records = data['waterPosition']
        current_type = True
        
        for i in range(1, len(records)-1):
            current_x, current_y = float(records[i]['x']), float(records[i]['y'])
            prev_x, prev_y = float(records[i-1]['x']), float(records[i-1]['y'])
            next_x, next_y = float(records[i+1]['x']), float(records[i+1]['y'])
            if prev_y <= current_y and next_y < current_y and current_type:
                high_points.append({
                    'x': i,
                    'y': current_y
                    })
                current_type = not current_type
            elif prev_y >= current_y and next_y > current_y and not current_type:
                low_points.append({
                    'x': i,
                    'y': current_y
                    })
                current_type = not current_type
        length = min([len(low_points), len(high_points)])
        distances = [{'x': i, 'y': high_points[i]['y'] - low_points[i]['y']} for i in range(length)]
        level_measures = {
            'low_points': low_points,
            'high_points': high_points,
            'distances': distances,
            'angle_tan': fitting_straight_line([item['x'] for item in distances], [item['y'] for item in distances])
        }

    if play[0].game_play.game_level.id == 3:
        animals = ['animal3', 'animal4', 'animal5']
        for ind, animalname in enumerate(animals):
            if data[animalname]:
                level_measures[animalname] = {
                    'minimumclicks': ind + 3,
                    'userclicks': len(data[animalname]),
                    'correct_rate': float(ind + 3) / len(data[animalname]),
                    'total_time': float(data[animalname][-1]['time']) - float(data[animalname][0]['time'])
                }

    if play[0].game_play.game_level.id == 4:
        crops = ['crop0use', 'crop1use', 'crop2use']
        for cropuse_label in crops:
            if data[cropuse_label]:
                series = [int(entry['serial']) for entry in data[cropuse_label] if int(entry['serial'])!=-1]
                correct_series = [v for ind, v in enumerate(series) if (ind==0 and v==1) or (ind>0 and v==series[ind-1]+1)]
                level_measures[cropuse_label] = {
                    'correct_rate': float(len(correct_series)) / len(series)
                }
    #print "level_measures: "+str(level_measures)
    alert_message = {
        'level_measures': level_measures,
        'alerts': alerts
    }
    return alert_message

def play_vis(play_id):
    """
        play_id is the primary key in PlayerGamePlay Table
        obtain the data needed for the virtualization from the db by yourself
    """
    ret_data = {}
    #play = PlayerGameDataRecord.objects.get(id=int(play_id))

    plays = PlayerGamePlay.objects.filter(id = play_id)
    play_records = PlayerGameDataRecord.objects.filter(game_play=plays)
    play_record = play_records[0]
    print "yes"
    data = json.loads(play_record.data)
    print data
    alerts = ''

    syncs = None
    ret_data['created']= "2013-06-13T08:39:57"
    ret_data['data']=data
    ret_data['id']= play_record.game_play.game_level.id

    ret_data['is_checked'] = "true"
    ret_data['level'] = "/api/v1/level/"+str(play_record.game_play.game_level.id)+"/"
    ret_data['patient'] = "/api/v1/user/2/"
    ret_data['resource_uri'] = "/api/v1/patientlevelrecord/1/"
    print ret_data
    return ret_data

def fitting_straight_line(x_array, y_array):
    if x_array and y_array:
        coeff = polyfit( x_array, y_array, 1)
        return coeff[0]
    else:
        return None
    # return 0.5


    # print coeff
    # print arctan2(coeff[0], 1)  * 180 / pi
    # return arctan2(coeff[0], 1)  * 180 / pi

