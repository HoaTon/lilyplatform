from django.conf.urls import patterns, url, include

from .game_api import *
from .views import *

user_urls = patterns('',
    url(r'^/(?P<username>[0-9a-zA-Z_-]+)/$', UserDetail.as_view(), name='user-detail'),
)

staff_urls = patterns('',
    url(r'^/(?P<pk>\d+)/players$', StaffPlayerList.as_view(), name='staffplayer-list'),
    url(r'^/(?P<pk>\d+)$', StaffDetail.as_view(), name='staff-detail')
)

player_urls = patterns('',
    url(r'^/(?P<pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/plans$', PlayerGamePlanList.as_view(), name='playergameplan-list'),
    url(r'^/(?P<pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/feedbacks$', PlayerGameFeedbackList.as_view(), name='playergamefeedback-list'),
    url(r'^/(?P<pk>\d+)/game_levels/(?P<gamelevel_uuid>[0-9a-zA-Z_-]+)/plays$', PlayerGamelevelPlayList.as_view(), name='playergamelevelplay-list'),
    url(r'^/(?P<pk>\d+)/game_levels/(?P<gamelevel_uuid>[0-9a-zA-Z_-]+)/data$', PlayerGamelevelPlayStats.as_view(), name='playergamelevelplay-stats'),
    url(r'^/(?P<pk>\d+)/games$', PlayerGameList.as_view(), name='playergame-list'), # to be implemented
    url(r'^/(?P<pk>\d+)$', PlayerDetail.as_view(), name='player-detail'),
    url(r'^/(?P<pk>\d+)/avatar$', PlayerAvatarList.as_view(), name='playeravatar-list'),
    url(r'^$', PlayerList.as_view(), name='player-list')
)

register_urls = patterns('',
    url(r'^$', PlayerRegistration.as_view(), name='player-registration')
)

leaderboard_urls = patterns('',
    url(r'^/(?P<game_level_uuid>.+)/$', view=leaderboard, name='leaderboard')
)


game_urls = patterns('',
    url(r'^/(?P<uuid>[0-9a-zA-Z_-]+)$', GameDetail.as_view(), name='game-detail'),
)

play_urls = patterns('',
    url(r'^/(?P<pk>\d+)/datarecords$', PlayDatarecordList.as_view(), name='playdatarecord-list'),
    url(r'^/(?P<pk>\d+)/data$', PlayDatarecordStats.as_view(), name='playdatarecord-stats'),
    url(r'^/(?P<pk>\d+)$', PlayDetail.as_view(), name='play-detail'),
    url(r'^$', PlayList.as_view(), name='play-list')
)

datarecord_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', DataRecordDetail.as_view(), name='datarecord-detail'),
    url(r'^$', DataRecordList.as_view(), name='datarecord-list')
)

feedback_urls = patterns('',
    url(r'^$', FeedbackList.as_view(), name='feedback-list')
)

plan_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PlanDetail.as_view(), name='plan-detail'),
    url(r'^$', PlanList.as_view(), name='plan-list')
)

topic_urls = patterns('',
    url(r'^/(?P<topic_pk>\d+)/threads/(?P<thread_pk>\d+)/comments$', TopicThreadComment.as_view(), name='topic-thread-comment'),
    url(r'^/(?P<pk>\d+)/threads$', TopicThread.as_view(), name='topic-thread'),
    url(r'^/(?P<pk>\d+)$', TopicDetail.as_view(), name='topic-detail'),
    url(r'^$', TopicList.as_view(), name='topic-list')
)

thread_urls = patterns('',
    url(r'^/(?P<pk>\d+)/comments$', ThreadComment.as_view(), name='thread-comment'),
    url(r'^/(?P<pk>\d+)$', ThreadDetail.as_view(), name='thread-detail'),
    url(r'^$', ThreadList.as_view(), name='thread-list')
)

comment_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', CommentDetail.as_view(), name='comment-detail'),
    url(r'^$', CommentList.as_view(), name='comment-list')
)

gameplatform_urls = patterns('',
    url(r'^users', include(user_urls)),
    url(r'^staffs', include(staff_urls)),
    url(r'^players', include(player_urls)),
    url(r'^games', include(game_urls)),
    url(r'^plays', include(play_urls)),
    url(r'^datarecords', include(datarecord_urls)),
    url(r'^feedbacks', include(feedback_urls)),
    url(r'^plans', include(plan_urls)),
    url(r'^topics', include(topic_urls)),
    url(r'^threads', include(thread_urls)),
    url(r'^comments', include(comment_urls)),

    url(r'^register', include(register_urls)),
    url(r'^leaderboard', include(leaderboard_urls)),
)
