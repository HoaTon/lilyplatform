import os
from StringIO import StringIO
from django.conf import settings
from django.core.files.images import ImageFile
from django.shortcuts import render, redirect
from django.utils import simplejson
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from .models import *
from datetime import timedelta, date
from django.db.models import Count, Min, Sum, Avg
from django.core import serializers
import json


def home_view(request):
    if not request.user.is_authenticated():
        return render_to_response("homepage.html", {
            }, RequestContext(request))

    if request.user.userprofile.user_type == 'staff':
        staff = get_object_or_404(Staff, user=request.user)
        staff_org = staff.organization
        org_games = staff_org.game_set.all()
        staff_players = Player.objects.filter(doctors__in=[staff, ])
        return render_to_response("dataapp/dashboard.html", {
            'org_games': org_games,
            'gender_choices': GENDER_CHOICES,
            'player_role_choices': PLAYER_ROLE_CHOICES,
            'agegroup_choices': AGEGROUP_CHOICES,
            'staff_players': staff_players,
            }, RequestContext(request))

    return render_to_response("dataapp/error.html", {
        }, RequestContext(request))


@login_required
def player_game_view(request, player_pk, game_uuid):
    player = get_object_or_404(Player, pk=player_pk)
    game = get_object_or_404(Game, uuid=game_uuid)
    return render_to_response("dataapp/playergamepage.html", {
        'player': player,
        'game': game,
        }, RequestContext(request))


def apidoc(request):
    return render_to_response("dataapp/apidoc.html", {
        }, RequestContext(request))

def apireference(request):
    return render_to_response("dataapp/API Reference.html", {
        }, RequestContext(request))

def statistics(request):
    return render_to_response("dataapp/statistics.html", {
        }, RequestContext(request))

def user_statistics(request):
    return render_to_response("dataapp/user_statistics.html", {
        }, RequestContext(request))

def user_statistics_table(request):
    game = request.POST.get('game')
    age_lower = request.POST.get('age_lower')
    age_upper = request.POST.get('age_upper')
    play_date = request.POST.get('date')

    if play_date != '':
        play_date = datetime.strptime(play_date, "%Y-%m-%d").date()

    if age_lower:
        age_lower = int(age_lower)

    else:
        age_lower = 0

    if age_upper:
        age_upper = int(age_upper)

    else:
        age_upper = 1000

    today = date.today()
    date_start = date(today.year - age_upper - 1, today.month, today.day) + timedelta(days = 1)
    date_end = date(today.year - age_lower, today.month, today.day)

    duration = request.POST.get('duration')

    players = Player.objects.filter(birthdate__gt = date_start, birthdate__lte = date_end)
    games = Game.objects.filter(name__contains=game)
    game_levels = GameLevel.objects.filter(game__in=games)
    playergameplays = PlayerGamePlay.objects.filter(player__in=players, game_level__in=game_levels)

    if date:
        playergameplays = playergameplays.filter(play_time__contains=play_date)

    return render_to_response("dataapp/user_statistics_table.html", {
        'playergameplays': playergameplays
        }, RequestContext(request))

def game_statistics(request):
    return render_to_response("dataapp/game_statistics.html", {
        }, RequestContext(request))

def game_statistics_table(request):
    #game_list = []
    #games = Game.objects.all()

    game_dict = PlayerGamePlay.objects.values('game_level__game__name').annotate(total_players=Count('player__id', distinct=True)).order_by('-total_players')
    '''
    for game in games:
        game_dict = {}
        game_dict['name'] = game.name
        satisfaction = 0
        game_list.append(game_dict)

    print game_list
    '''
    return render_to_response("dataapp/game_statistics_table.html", {
        'game_dict': game_dict
        }, RequestContext(request))

def game_level_statistics(request):
    return render_to_response("dataapp/game_level_statistics.html", {
        }, RequestContext(request))

def game_level_statistics_table(request):
    game_list = []
    games = Game.objects.all()
    #customers = Stockflow.objects.filter(in_qty__gt=0).values('customer__name', 'customer__address', 'customer__phone_number').annotate(total_transactions=Sum('in_qty')).order_by('-total_transactions')[0:k]
    game_dict = PlayerGamePlay.objects.values('game_level__name', 'game_level__game__name').annotate(total_players=Count('player__id', distinct=True)).order_by('-total_players')
    #print "DKMM"
    print game_dict
    #for game in games:
        #game_dict = {}
        #game_dict['name'] = game.name
        #satisfaction = 0
        #game_list.append(game_dict)

    #print game_list
    return render_to_response("dataapp/game_level_statistics_table.html", {
        'game_dict': game_dict,
        }, RequestContext(request))

def analysis(request):
    return render_to_response("dataapp/analysis.html", {
        }, RequestContext(request))

def leaderboard(request, game_level_uuid):
    game_level = GameLevel.objects.filter(uuid=game_level_uuid)[0]
    leaders = PlayerGamePlay.objects.filter(game_level=game_level).values('player__user__username').annotate(total_score=Sum('score')).order_by('-total_score')
    #data = serializers.serialize('json', leaders).only('player__user__username','total_score')
    #leaderboard = {'leaders': leaders}
    leaderboard = []

    for l in list(leaders):
        if l["total_score"] and len(leaderboard) < 3:
            leaderboard.append({"username": l["player__user__username"], "total_score": l["total_score"]})
    data =  json.dumps(list(leaderboard))
    return HttpResponse(data, mimetype='application/json')

def profile(request):
    if request.user.is_authenticated():
        return render_to_response("dataapp/profile.html", {
            'profile': request.user.get_profile()
            }, RequestContext(request))
    return redirect('admin:index')


def blogpost(request):
    if request.user.is_authenticated():
        return render_to_response("dataapp/blogpost.html", {
            }, RequestContext(request))
    return redirect('admin:index')


@csrf_exempt
def upload_image(request):
    user = request.user
    if (request.method != 'POST' or not request.is_ajax() or
        not user.is_authenticated()):
        raise Http404
    fname = request.GET.get('qqfile', '')
    try:
        fname = '.'.join(fname.split('.')[-2:])
    except IndexError:
        return HttpResponse(
            simplejson.dumps({'success': False}),
            mimetype='application/json')
    f = StringIO()
    while True:
        buf = request.read(512 * 1024)
        if buf:
            f.write(buf)
        else:
            break
    profile = user.get_profile()
    pf = ProfilePhoto(
            profile=profile,
            title=fname.split('.')[0],
            image=ImageFile(f, name=fname))
    pf.save()
    return HttpResponse(
        simplejson.dumps({'success': True, 'url': pf.image.url}),
        mimetype='application/json')

def crossdomain(request):
    return HttpResponse(open('crossdomain.xml').read(), mimetype="application/xhtml+xml")
